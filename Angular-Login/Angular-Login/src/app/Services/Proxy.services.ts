import {Injectable} from '@angular/core';
import enumerate = Reflect.enumerate;
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {JsonFormatter} from 'tslint/lib/formatters';
import {stringify} from 'querystring';
import {log, promisify} from 'util';
import {Observable} from 'rxjs';

// The Web Response Class
export class WebResponse<T> {
  public Response: T ;
  public StatusCode: boolean;
  public Message: string ;

  constructor(response: T, statusCode: boolean , message: string) {
    this.Response = response;
    this.StatusCode = statusCode;
    this.Message = message || null;
  }
}

// The ProxyServices Class
@Injectable({providedIn: 'root'})
export class ProxyServices {
  public Uid: number;
  public serverUrl = 'http://localhost:54298/api/';

  constructor(private http: HttpClient) {
  }
  // Login Function
  public async Login(Username: string, password: string): Promise<WebResponse<number>> {

    const postdata = {UserName: Username, Password: password};
    let response: WebResponse<number>;
    // tslint:disable-next-line:max-line-length
    await this.http.post<Observable<WebResponse<number>>>(this.serverUrl.concat('User/Login/'), postdata).toPromise().then((data: any) => {
      response = data; });

    if (response == null) {
    console.log('fail');
    response = new WebResponse<number>(1, false, 'ServerFailure');
  }
    this.Uid = response.Response;
    return response;
  }

  // Logout Function
  public Logout(): void {
    this.http.get(this.serverUrl.concat('User/Logout/', this.Uid.toString()));
  }

  // SignUp Function
  public async SignUp(UserName: string, password: string, email: string): Promise<WebResponse<number>> {
    const postdata = {Username: UserName, Password: password, Email: email};
    let response: WebResponse<number>;
    await this.http.post<Observable<WebResponse<number>>>(this.serverUrl.concat('User/SignUp/'), postdata).toPromise().then((data: any) => {
      response = data; });

    if (response == null) {
      console.log('fail');
      response = new WebResponse<number>(1, false, 'ServerFailure');
    }
    return response;
  }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Configuration;

namespace HttpServerSide
{
    //Type Of Response
    public class WebResponse<ResponseType>
    {
        public ResponseType Response { get; set; }
        public bool StatusCode { get; set; }
        public string Message { get; set; }
        
        public WebResponse(ResponseType response, bool statusCode, string message)
        {
            this.Response = response;
            this.StatusCode = statusCode;
            this.Message = message;
        }
        
        public WebResponse()
        {

        }


}



    //Class That "Speaks" With DataBase
    public class HttpServerSide
    {
        public MySqlConnection Conn { get; set; } = null;
        public HttpServerSide()
        {
            var connStr = "server=127.0.0.1;user=root;database=angular;port=3306;password=gabik.com;";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            Conn = conn;
        }

        ~HttpServerSide()
        {
            if (Conn != null)
                Conn.Close();
        }



        //a Pathetic SqlInjections filter
        private bool CheckForInjections(string str)
        {
            if (str.Contains(";") || str.Contains("=") || str.Contains("'")) { return true; }
            return false;
        }

        //Login
        public WebResponse<int> Login(string UserName, string Password)
        {
            if (CheckForInjections(UserName) || CheckForInjections(Password))
            {
                return new WebResponse<int>(0, false, "The Input Has Prohibited Symbols");
            }
            WebResponse<int> response = new WebResponse<int>(0, false, "The Password or UserName is inccorect");
            string sql = $"SELECT uid FROM user WHERE UserName = '{UserName}' AND Password = '{Password}' ;";
            int logon;

            MySqlTransaction Trans;
            Trans = Conn.BeginTransaction();

            try
            {
                using (MySqlCommand login = new MySqlCommand(sql, Conn))
                {
                    using (MySqlDataReader reader = login.ExecuteReader())
                    {
                        if (reader.HasRows && reader.Read())
                        {
                            int uid = Convert.ToInt32(reader["uid"]);
                            response = new WebResponse<int>(uid, true, "Success");
                        }

                    }
                }
                sql = $"SELECT IsOnline FROM user WHERE uid ='{response.Response}';";
                using (MySqlCommand login = new MySqlCommand(sql, Conn))
                {
                    using (MySqlDataReader reader = login.ExecuteReader())
                    {
                        reader.Read();
                        logon = Convert.ToInt32(reader["IsOnline"]);
                        if (logon == 1)
                        {
                            return new WebResponse<int>(0, false, "The User Has Already Logon");
                        }

                    }
                }
                // sql = $"UPDATE user SET IsOnline = '1' WHERE uid ='{response.Response}';";
                //using (MySqlCommand login = new MySqlCommand(sql, Conn))
                //{
                //     login.ExecuteNonQuery();
                //}
                Trans.Commit();
            }

            catch (Exception ex)
            {
                Trans.Rollback();
                response = new WebResponse<int>(0, false, ex.Message);
                if (ex.Message == "Invalid attempt to access a field before calling Read()")
                {
                    response = new WebResponse<int>(0, false, "The Password or UserName is inccorect");
                }

            }

            Conn.Close();
            return response;
        }

        //LogOut
        public void Logout(int uid)
        {
            string sql = $"UPDATE user SET IsOnline = '0' Where uid = '{uid}'";
            try
            {
                using (MySqlCommand login = new MySqlCommand(sql, Conn))
                {
                    login.ExecuteNonQuery();
                }
            }
            catch { }
        }


        //SignUp
        public WebResponse<int> SignUp(string username, string password, string email)
        {
            if (CheckForInjections(username) || CheckForInjections(password) || CheckForInjections(email))
            {
                return new WebResponse<int>(0, false, "The Input Has Prohibited Symbols");
            }

            WebResponse<int> response;
            int id;
            string sql = $"SELECT uid FROM user WHERE UserName = '{username}';";
            try
            {
                using (MySqlCommand login = new MySqlCommand(sql, Conn))
                {
                    using (MySqlDataReader reader = login.ExecuteReader())
                    {
                        if (reader.HasRows && reader.Read())
                        {
                            return new WebResponse<int>(0, false, "The UserNameIsTaken");
                        }

                    }

                }
                sql = $"INSERT INTO user (UserName,Password,Email) VALUES('{username}','{password}','{email}'); Select last_insert_id()";
                using (MySqlCommand cmd = new MySqlCommand(sql, Conn))
                {
                    id = Convert.ToInt32(cmd.ExecuteScalar());
                }
                response = new WebResponse<int>(id, true, "Success");
            }
            catch (Exception ex)
            {
                response = new WebResponse<int>(0, false, ex.Message);
            }
            return response;
        }

    }
}

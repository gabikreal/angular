import {Component, OnInit} from '@angular/core';
import {TodosService} from '../shared/todos.service';
import {delay} from 'rxjs/operators';


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
  // tslint:disable-next-line:no-shadowed-variable
  constructor(private TodosService: TodosService) { }

  private loading = true;

  ngOnInit() {
    this.TodosService.fetchTodos().pipe(delay(500)).subscribe( () => {this.loading = false; });
  }
  onChange(id: number) {
    this.TodosService.onToggle(id);
  }
  removeTodo(id: number) {
    this.TodosService.removeTodo(id);
  }
}

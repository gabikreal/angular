﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HttpServerSide;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using System.Web.Http;

namespace Server.Controllers
{
    public class LoginForm
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class SignUpForm
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }

    [RoutePrefix("api/User")]
    [EnableCors("*", "*", "*")]
    public class UserController : ApiController
    {
        [HttpPost,Route("Login")]
        public WebResponse<int> Login([FromBody]LoginForm lg)
        {
            return new HttpServerSide.HttpServerSide().Login(lg.Username,lg.Password);
        }
        [HttpGet,Route("Logout/{Uid}")]
        public void Logout(int Uid)
        {
            new HttpServerSide.HttpServerSide().Logout(Uid);
        }
        [HttpPost,Route("SignUp")]
        public WebResponse<int> SignUp([FromBody]SignUpForm sp)
        {
            return new HttpServerSide.HttpServerSide().SignUp(sp.Username, sp.Password, sp.Email);
        }
    }
}
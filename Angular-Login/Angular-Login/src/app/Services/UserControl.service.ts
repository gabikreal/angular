import { Injectable} from '@angular/core';
import {ProxyServices} from './Proxy.services';
import {WebResponse} from './Proxy.services';
import {delay} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class UserControlService {
  constructor(private proxyServices: ProxyServices) {}
  public async Login(Username: string, Password: string): Promise<WebResponse<number>> {
    const res = await this.proxyServices.Login(Username, Password);
    return res;
  }

  public Logout(): void {
  this.proxyServices.Logout();
  }

  public async SignUp(UserName: string, Password: string, Email: string): Promise<WebResponse<number>> {
    return await this.proxyServices.SignUp(UserName, Password, Email);
  }
}

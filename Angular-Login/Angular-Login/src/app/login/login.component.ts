import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserControlService} from '../Services/UserControl.service';
import {WebResponse} from '../Services/Proxy.services';
import {delay, timeout} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', './util.css', './main.css']
})

export class LoginComponent implements OnInit {
  @Output() OnSwitch = new EventEmitter<string>();
  result: WebResponse<number> = new WebResponse<number>(1, true, '');
  UserName = '';
  Password = '';

 constructor(private userControl: UserControlService) { }

 SignUp() {
  this.OnSwitch.emit('SignUp');
 }

 async Login() {
   if (this.UserName === '' || this.Password === '') {
     return;
   }
   this.result =  await this.userControl.Login(this.UserName, this.Password);
   console.log(this.result);
   if (this.result.StatusCode) {
     this.OnSwitch.emit('Main');
   }
 }

 ngOnInit() {
  }
}

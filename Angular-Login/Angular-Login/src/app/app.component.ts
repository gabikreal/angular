import { Component } from '@angular/core';
import {UserControlService} from './Services/UserControl.service';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {}
  title = 'Angular-Login';
  CurrentPage = 'Login';
  ChangePage(pageName: string) {
    this.CurrentPage = pageName;
  }

}

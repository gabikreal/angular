import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserControlService} from '../Services/UserControl.service';
import {WebResponse} from '../Services/Proxy.services';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss', './main.css', './util.css']
})
export class SignUpComponent implements OnInit {

  constructor(private userControl: UserControlService) { }
  @Output() OnSwitch = new EventEmitter<string>();
  UserName = '';
  Password = '';
  PasswordConfirm = '';
  Email = '';

  public response = new WebResponse<number>(0, true, '');

  ngOnInit() {
  }
  Login() {
    this.OnSwitch.emit('Login');
  }
  async SignUp(): Promise<void> {
    if (this.UserName.length < 6) {
      this.response.StatusCode = false; this.response.Message = 'Username has to be 6 symbols at least';
      return;
    }
    if (this.Password.length < 6) {
      this.response.StatusCode = false; this.response.Message = 'Password has to be 6 symbols at least';
      return ;
    }
    if (this.Password !== this.PasswordConfirm) {
      this.response.StatusCode = false; this.response.Message = 'Password Confirm does not match the Password';
      return ;
    }
    if (!this.Email.includes('@') || this.Email.length < 6) {
      this.response.StatusCode = false; this.response.Message = 'Enter a valid Email address';
      return ;
    }
    this.response =  await this.userControl.SignUp(this.UserName, this.Password, this.Email);
    if (this.response.StatusCode === false) {
      return ;
    }
    this.OnSwitch.emit('Login');
  }
}

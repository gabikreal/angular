import {Component, EventEmitter, HostListener, OnDestroy, OnInit, Output} from '@angular/core';
import {UserControlService} from '../Services/UserControl.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit{

  constructor(private UserControl: UserControlService) { }

  ngOnInit() {
  }
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    this.UserControl.Logout();
  }

}
